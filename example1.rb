require 'gtk3'

class RubyApp < Gtk::Window

  def initialize
    super

    set_title 'Example 1'
    signal_connect 'destroy' do
      Gtk.main_quit
    end

    set_default_size 600, 400

    set_window_position Gtk::WindowPosition::CENTER

    show
  end
end

RubyApp.new
Gtk.main
